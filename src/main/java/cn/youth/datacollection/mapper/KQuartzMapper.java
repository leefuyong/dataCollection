package cn.youth.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.youth.datacollection.entity.KQuartz;

public interface KQuartzMapper extends BaseMapper<KQuartz> {
}
