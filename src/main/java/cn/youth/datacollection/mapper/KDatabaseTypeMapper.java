package cn.youth.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.youth.datacollection.entity.KDatabaseType;

public interface KDatabaseTypeMapper extends BaseMapper<KDatabaseType> {
}
